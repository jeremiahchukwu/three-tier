#!/bin/bash
# sudo yum update -y
# sudo yum install -y amazon-cloudwatch-agent
# sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s
# cd /tmp
# sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
# sudo systemctl enable amazon-ssm-agent
# sudo systemctl start amazon-ssm-agent

# echo 'export HELLO_WORLD="Hello World!"' >> ~/.bash_profile

# sudo yum install httpd -y
# sudo systemctl enable httpd
# sudo systemctl start httpd


# echo "<html><body><div>This is a test webserver!</div></body></html>" > /var/www/html/index.html
sudo apt update -y
mkdir /tmp/ssm
cd /tmp/ssm
wget https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb
sudo dpkg -i amazon-ssm-agent.deb

sudo systemctl enable amazon-ssm-agent
sudo systemctl start amazon-ssm-agent
sudo apt install apache2 -y
sudo systemctl enable apache2
sudo systemctl start apache2
cat <<- EOF > /var/www/html/index.html
<!DOCTYPE html>
<head>
	<title>Hello World</title>
  <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">
	<style>
		body {
			background-color: #2D2D2D;
		}
		h1 {
			color: #C26356;
			font-size: 52px;
			font-family: Menlo, Monaco, fixed-width;
		}
		p {
			color: white;
			font-size: 34px;
			font-family: "Source Code Pro", Menlo, Monaco, fixed-width;
		}
		container {
			position: absolute;
			margin: auto;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			width: 52em;
			height: 10em;
		}
	</style>
</head>
<body>
	<container><h1>Hello World</h1>
	<p>We are live.</p></container>
</body>
</html>
EOF

nohup busybox httpd -f -p 80 &