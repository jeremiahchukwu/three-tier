locals {
  name   = "${var.name}"

  tags = {
    Name       = local.name
  }

  engine                = "mysql"
  engine_version        = "8.0.27"
  family                = "mysql8.0" # DB parameter group
  major_engine_version  = "8.0"      # DB option group
  instance_class        = "db.t2.micro"
  allocated_storage     = 10
  max_allocated_storage = 20
  port                  = 3306
}

data "aws_caller_identity" "current" {
}

# data "aws_ami" "amazon_linux" {
#   most_recent = true
#   owners = ["amazon"]
#   filter{
#     name = "name"
#     values = ["amzn2-ami-hvm-*-x86_64-gp2"]
#   }
# }

## amazon-linux 2
# data "aws_ami" "amazon_linux_2" {
#   owners      = ["amazon"]
#   most_recent = true

#   filter {
#     name   = "name"
#     values = [format("amzn2-ami-hvm-*")]
#   }
#   filter {
#     name   = "block-device-mapping.volume-type"
#     values = ["gp2"]
#   }
  
#   filter {
#     name   = "architecture"
#     values = ["x86_64"]
#   }
# }

module "vpc" {
  source = "./modules/vpc"
  cidr = "192.168.0.0/16"
  name = var.name
  private_subnets = ["192.168.2.0/24","192.168.4.0/24","192.168.5.0/24","192.168.6.0/24"]
  database_subnets = ["192.168.7.0/24","192.168.8.0/24"]
  public_subnets = ["192.168.1.0/24","192.168.3.0/24",]
  availability_zones = ["us-east-1a", "us-east-1b","us-east-1c","us-east-1d"]
}
module "iam" {
  source =   "./modules/iam"
  name = "${var.name}"
  
}
module "ssm" {
  source = "./modules/systemmanager"
  name   =  "${var.name}-ssm"
  iam_name = module.iam.iam_role_name
  linux_shell_profile = "date"
  ssm_document_name = "SSM-SessionManagerRunShell-assessment"
}
module "security" {
  depends_on = [
    module.vpc
  ]
  source = "./modules/security"
  name = "${var.name}"
  app_port = 80
  web_port  = 80
  vpc_id = module.vpc.id
}



#------------------------------------------------------------------------------
# Backend  App Server ASG, ALB, Security Group Configuration
#------------------------------------------------------------------------------


module "app_load_balancer" {
  depends_on = [
    module.vpc
  ]
  source = "./modules/loadbalancer"
  name = "${var.name}-APP"
  alb_security_groups = [module.security.app_alb]
  health_check_path = "/"
  vpc_id = module.vpc.id
  subnets = module.vpc.private_subnets
  alb_service_port = 80
  alb_target_protocol = "HTTP"
  internal = true

  # alb_security_groups = [module.security.ecs_tasks]
  # subnet = module.vpc.public_subnets
}
   
module "auto-scaling" {
  source = "./modules/asg"
  name_prefix = "${var.name}-APP"
  # image_id = data.aws_ami.amazon_linux_2.id
  # image_id = "ami-0c55b159cbfafe1f0"
  image_id = "ami-08c40ec9ead489470"
  instance_type = "t2.medium"
  iam_instance_profile = "${module.ssm.iam_instance_profile_name}"
  security_groups = ["${module.security.app_server}"]
  associate_public_ip_address = false
  user_data = "${file("${path.module}/user_script.sh")}"
  min_elb_capacity = 1
  max_size = 2
  min_size = 1
  desired_capacity = 1
  health_check_type = "ELB"
  vpc_zone_identifier = ["${module.vpc.private_subnets.*[2].id}","${module.vpc.private_subnets.*[3].id}"]
  target_group_arns = ["${module.app_load_balancer.aws_alb_target_group_arn}"]
  wait_for_capacity_timeout = null
  wait_for_elb_capacity = null
}


#------------------------------------------------------------------------------
# WAF Web Server ASG, ALB, Security Group Configuration
#------------------------------------------------------------------------------

# resource "aws_wafregional_rate_based_rule" "ipratelimit" {
#   name        = "${local.name}-app-global-ip-rate-limit"
#   metric_name = "wafAppGlobalIpRateLimit"
#   rate_key    = "IP"
#   rate_limit  = 2000
# }

# module "waf" {
#   source = "./modules/waf"
#   alb_arn       = module.frontend_load_balancer.aws_alb_target_group_arn
#   associate_alb = true
#   blocked_path_prefixes = ["/admin", "/password"]
#   allowed_hosts         = ["web"]
#   rate_based_rules = [aws_wafregional_rate_based_rule.ipratelimit.id]
#   web_acl_name        = "assessment-waf"
#   web_acl_metric_name = "IP-metric"
# }


#------------------------------------------------------------------------------
# Frontend  Web Server ASG, ALB, Security Group Configuration
#------------------------------------------------------------------------------


module "frontend_load_balancer" {
  depends_on = [
    module.vpc
  ]
  source = "./modules/loadbalancer"
  name = "${var.name}-WEB"
  alb_security_groups = [module.security.web_alb]
  health_check_path = "/"
  vpc_id = module.vpc.id
  subnets = module.vpc.public_subnets
  alb_service_port = 80
  alb_target_protocol = "HTTP"
  internal = false

  # alb_security_groups = [module.security.ecs_tasks]
  # subnet = module.vpc.public_subnets
}
   
module "web_auto_scaling_group" {
  source = "./modules/asg"
  name_prefix = "${var.name}-WEB"
  # image_id = data.aws_ami.amazon_linux_2.id
  # # Ubuntu Server 18.04 LTS (HVM), SSD Volume Type in us-east-1
  image_id = "ami-08c40ec9ead489470"
  instance_type = "t2.medium"
  iam_instance_profile = "${module.ssm.iam_instance_profile_name}"
  security_groups = ["${module.security.web_server}"]
  associate_public_ip_address = false
  user_data = "${file("${path.module}/user_script.sh")}"
  min_elb_capacity = 1
  max_size = 2
  min_size = 1
  desired_capacity = 1
  health_check_type = "ELB"
  vpc_zone_identifier = ["${module.vpc.private_subnets.*[0].id}","${module.vpc.private_subnets.*[1].id}"]
  target_group_arns = ["${module.frontend_load_balancer.aws_alb_target_group_arn}"]
  wait_for_capacity_timeout = null
  wait_for_elb_capacity = null

}

module "guardduty" {
  source = "git@github.com:cmdlabs/terraform-aws-guardduty.git"
  bucket_name = "s3-audit-someclient-guardduty"
  detector_enable = true
  is_guardduty_master = true
  has_ipset = true
  has_threatintelset = true

  ipset_activate = true
  ipset_format = "TXT"
  ipset_iplist = [
    "1.1.1.1",
    "2.2.2.2",
  ]

  threatintelset_activate = true
  threatintelset_format = "TXT"
  threatintelset_iplist = [
    "3.3.3.3",
    "4.4.4.4",
  ]

  # member_list = [{
  #   account_id   = "${data.aws_caller_identity.current.account_id}"
  #   member_email = "jeremiahchukwu@gmail.com"
  #   invite       = true
  # }]
}


#------------------------------------------------------------------------------
# Database Master, Read Replica Security Group Configuration
#------------------------------------------------------------------------------


################################################################################
# Master DB
################################################################################
# resource "aws_db_subnet_group" "default" {
#   name        = "${local.name}-subnet-group"
#   description = "RDS subnet group"
#   subnet_ids  = ["${module.vpc.database_subnets.*[0].id}","${module.vpc.database_subnets.*[1].id}"]
# }
# module "master" {
#   source = "./modules/rds"
#   identifier = "${local.name}-master"

#   engine               = local.engine
#   engine_version       = local.engine_version
#   family               = local.family
#   major_engine_version = local.major_engine_version
#   instance_class       = local.instance_class

#   allocated_storage     = local.allocated_storage
#   max_allocated_storage = local.max_allocated_storage

#   db_name  = "chima"
#   username = "chima"
#   port     = local.port

#   multi_az               = true
#   db_subnet_group_name   = aws_db_subnet_group.default.name
#   vpc_security_group_ids =  ["${module.security.rds}"]

#   maintenance_window              = "Mon:00:00-Mon:03:00"
#   backup_window                   = "03:00-06:00"
#   enabled_cloudwatch_logs_exports = ["general"]

#   # Backups are required in order to create a replica
#   backup_retention_period = 1
#   skip_final_snapshot     = true
#   deletion_protection     = false
#   storage_encrypted = false

#   tags = local.tags
# }

################################################################################
# Replica DB
################################################################################

# module "replica" {
#   source = "./modules/rds"

#   identifier = "${local.name}-replica"

#   # Source database. For cross-region use db_instance_arn
#   replicate_source_db    = module.master.db_instance_id
#   create_random_password = false

#   engine               = local.engine
#   engine_version       = local.engine_version
#   family               = local.family
#   major_engine_version = local.major_engine_version
#   instance_class       = local.instance_class

#   allocated_storage     = local.allocated_storage
#   max_allocated_storage = local.max_allocated_storage

#   port = local.port

#   multi_az               = false
#   vpc_security_group_ids = ["${module.security.rds}"]

#   maintenance_window              = "Tue:00:00-Tue:03:00"
#   backup_window                   = "03:00-06:00"
#   enabled_cloudwatch_logs_exports = ["general"]

#   backup_retention_period = 0
#   skip_final_snapshot     = true
#   deletion_protection     = false
#   storage_encrypted = false

#   tags = local.tags
# }




