variable "name" {
  description = "the name of your stack, e.g. \"demo\""
}

# variable "environment" {
#   description = "the name of your environment, e.g. \"prod\""
# }

variable "cidr" {
  type = string
  description = "The CIDR block for the VPC."
}

variable "public_subnets" {
  type = list
  description = "List of public subnets"
}

variable "private_subnets" {
  type = list
  description = "List of private subnets"
}

variable "database_subnets" {
  type = list
  description = "List of public subnets"
}

variable "availability_zones" {
  description = "List of availability zones"
}





# variable "network_interface_id" {
#   type = string
#   default = "network_id_from_aws"
# }

# variable "ami" {
#     type = string
#     default = "ami-0d75513e7706cf2d9"
# }

# variable "instance_type" {
#     type = string
#     default = "t2.micro"
# }
# variable "sg_name" {
#   type = string
#   default = "sec_assessment"
# }
# variable "sg_description"{
#   type = string
#   default = "Web server firewall"
# }

# variable "vpc_id"{
#   type = string
#   # default = "vpc-0a862e76be454294e"
# }
# variable "subnet_id" {
#   type = string
#   # default = "subnet-09f12d767bd8f5b7c"
# }