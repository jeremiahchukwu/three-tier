# output "ec2instance" {
#   value = aws_instance.web_server.public_ip
# }
# output "sshkey" {
#   value = tls_private_key.oskey.public_key_openssh
# }


output "id" {
  value = aws_vpc.main.id
}

output "public_subnets" {
  value = aws_subnet.public
}

output "private_subnets" {
  value = aws_subnet.private
}
output "database_subnets" {
  value = aws_subnet.db_private
  # value = tomap({
  #   for name, subnet in aws_subnet.db_private: name => subnet.id
  # })
}