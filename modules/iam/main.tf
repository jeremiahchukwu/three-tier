# Session Manager IAM Instance Profile
#
# https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-getting-started-instance-profile.html

# https://www.terraform.io/docs/providers/aws/r/iam_instance_profile.html
data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "default" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_iam_instance_profile" "ssm" {
  name = "${var.name}-session-manager"
  role = aws_iam_role.ssm.name
  path = var.iam_path
}

# https://www.terraform.io/docs/providers/aws/r/iam_role.html
resource "aws_iam_role" "ssm" {
  name               = "${var.name}-session-manager"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
  path               = var.iam_path
  description        = var.description
  tags               = merge({ "Name" = "${var.name}-session-manager" }, var.tags)
}



# https://www.terraform.io/docs/providers/aws/r/iam_policy.html
resource "aws_iam_policy" "ssm" {
  name        = "${var.name}-session-manager"
  policy      = var.iam_policy == "" ? data.aws_iam_policy.default.policy : var.iam_policy
  path        = var.iam_path
  description = var.description
}

# https://www.terraform.io/docs/providers/aws/r/iam_role_policy_attachment.html
resource "aws_iam_role_policy_attachment" "default" {
  role       = aws_iam_role.ssm.name
  policy_arn = aws_iam_policy.ssm.arn
}

# locals {
#   iam_name   = "${var.name}-session-manager"
#   iam_policy = var.iam_policy == "" ? data.aws_iam_policy.default.policy : var.iam_policy
# }




# resource "aws_iam_role" "ecs_task_execution_role" {
#   name = "${var.name}-ecsTaskExecutionRole"

#   assume_role_policy = <<EOF
#     {
#       "Version": "2012-10-17",
#       "Statement": [
#         {
#           "Action": "sts:AssumeRole",
#           "Principal": {
#             "Service": "ecs-tasks.amazonaws.com"
#           },
#           "Effect": "Allow",
#           "Sid": ""
#         }
#       ]
#     }
#     EOF
#   }

# resource "aws_iam_role" "ecs_task_role" {
#   name = "${var.name}-ecsTaskRole"
#   assume_role_policy = <<EOF
#     {
#       "Version": "2012-10-17",
#       "Statement": [
#         {
#           "Action": "sts:AssumeRole",
#           "Principal": {
#             "Service": "ecs-tasks.amazonaws.com"
#           },
#           "Effect": "Allow",
#           "Sid": ""
#         }
#       ]
#     }
#     EOF
#   }

# resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
#   role       = aws_iam_role.ecs_task_execution_role.name
#   policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
# }
