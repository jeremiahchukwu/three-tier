output "iam_role_arn" {
  value       = aws_iam_role.ssm.arn
  description = "The Amazon Resource Name (ARN) specifying the IAM Role."
}

output "iam_role_create_date" {
  value       = aws_iam_role.ssm.create_date
  description = "The creation date of the IAM Role."
}

output "iam_role_unique_id" {
  value       = aws_iam_role.ssm.unique_id
  description = "The stable and unique string identifying the IAM Role."
}

output "iam_role_name" {
  value       = aws_iam_role.ssm.name
  description = "The name of the IAM Role."
}

output "iam_role_description" {
  value       = aws_iam_role.ssm.description
  description = "The description of the IAM Role."
}

output "iam_policy_id" {
  value       = aws_iam_policy.ssm.id
  description = "The IAM Policy's ID."
}

output "iam_policy_arn" {
  value       = aws_iam_policy.ssm.arn
  description = "The ARN assigned by AWS to this IAM Policy."
}

output "iam_policy_description" {
  value       = aws_iam_policy.ssm.description
  description = "The description of the IAM Policy."
}

output "iam_policy_name" {
  value       = aws_iam_policy.ssm.name
  description = "The name of the IAM Policy."
}

output "iam_policy_path" {
  value       = aws_iam_policy.ssm.path
  description = "The path of the IAM Policy."
}

output "iam_policy_document" {
  value       = aws_iam_policy.ssm.policy
  description = "The policy document of the IAM Policy."
}
# output "execution_role_arn" {
#   value = aws_iam_role.ecs_task_execution_role.arn
# }

# output "task_role_arn" {
#   value = aws_iam_role.ecs_task_role.arn
# }