variable "name" {
  description = "the name of your stack, e.g. \"demo\""
}

variable "subnets" {
  description = "Comma separated list of subnet IDs"
}

variable "vpc_id" {
  description = "VPC ID"
}
variable "internal" {
  description = "The ALB is internal"
  type = bool
  default = false
}
variable "alb_target_protocol"{
  type = string
  description = "Application protocol"
}

variable "alb_service_port"{
  type = number
  description = "Application LB PORT"
}
variable "target_type" {
  type = string
  description = "Application Load Balancer Instance Type"
  default = "instance"
  
}
variable "alb_security_groups" {
  type = list
  description = "Comma separated list of security groups"
}

# variable "alb_tls_cert_arn" {
#   description = "The ARN of the certificate that the ALB uses for https"
# }

variable "health_check_path" {
  description = "Path to check if the service is healthy, e.g. \"/status\""
}