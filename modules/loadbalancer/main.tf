resource "aws_lb" "main" {
  name               = "${var.name}-alb"
  ip_address_type    =  "ipv4"
  internal           = var.internal
  load_balancer_type = "application"
  security_groups    = var.alb_security_groups
  subnets            = var.subnets.*.id
  # cross_zone_load_balancing   = true
  enable_deletion_protection = false
  tags = {
    Name        = "${var.name}-alb"
  }
}

resource "aws_alb_target_group" "main" {
  name        = "${var.name}-tg"
  port        =  "${var.alb_service_port}"
  protocol    = "${var.alb_target_protocol}"
  vpc_id      = var.vpc_id
  target_type = var.target_type

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = var.health_check_path
    unhealthy_threshold = "2"
  }

  tags = {
    Name        = "${var.name}-tg"
    # Environment = var.environment
  }
}
// target_id generates a random 28-character ID for use as the unique ID for
// our target group. The byte length is actually 14 bytes so that it can be
// rendered as hex.
resource "random_id" "target_id" {
  byte_length = 14
}

# resource "aws_alb_target_group" "http" {
#   # count    = "${lookup(map("true", "1"), var.enable_alb, "0")}"
#   name        = "${var.name}-tg"
#   port        =  "${var.alb_service_port}"
#   protocol    = "${var.alb_target_protocol}"
#   vpc_id      = var.vpc_id
#   target_type = var.target_type
#   health_check {
#     healthy_threshold   = "3"
#     interval            = "30"
#     protocol            = "${var.alb_target_protocol}"
#     matcher             = "200"
#     timeout             = "3"
#     path                = var.health_check_path
#     unhealthy_threshold = "2"
#   }

#   tags = {
#     Name        = "${var.name}-${random_id.target_id.hex}-tg"
#   }
# }


# Redirect to http listener
# resource "aws_alb_listener" "http" {
#   # count    = "${lookup(map("true", "1"), var.enable_alb, "0")}"
#   load_balancer_arn = aws_lb.main.id
#   port              = 80
#   protocol          = "HTTP"

#   default_action {
#     target_group_arn = aws_alb_target_group.main.id
#     type             = "forward"
#   }
#   # default_action {
#   #   type = "redirect"

#   #   redirect {
#   #     port        = 443
#   #     protocol    = "HTTPS"
#   #     status_code = "HTTP_301"
#   #   }
#   # }
# }

# Redirect to http listener
resource "aws_alb_listener" "main" {
  load_balancer_arn = aws_lb.main.id
  port              = "${var.alb_service_port}"
  protocol          = "${var.alb_target_protocol}"

  default_action {
    target_group_arn = aws_alb_target_group.main.id
    type             = "forward"
  }
}
# resource "aws_alb_listener_rule" "bitbucket" {
#   listener_arn = "${aws_alb_listener.atlassian.arn}"
#   priority     = 99

#   action {
#     type = "forward"
#     target_group_arn = "${aws_alb_target_group.bitbucket.arn}"
#   }

#   condition {
#     field  = "host-header"
#     values = ["bitbucket.example.com"]
#   }
# }


# # Redirect traffic to target group
# resource "aws_alb_listener" "https" {
#     load_balancer_arn = aws_lb.main.id
#     port              = 443
#     protocol          = "HTTPS"

#     ssl_policy        = "ELBSecurityPolicy-2016-08"
#     certificate_arn   = var.alb_tls_cert_arn

#     default_action {
#         target_group_arn = aws_alb_target_group.main.id
#         type             = "forward"
#     }
# }

