variable "name" {
  type = string
  description = "the name of your stack, e.g. \"demo\""
}
variable "iam_name" {
  default     = ""
  type        = string
  description = "The Iam Role Name."
}
variable "iam_path" {
  default     = "/"
  type        = string
  description = "Path in which to create the IAM Role and the IAM Policy."
}
variable "ssm_document_name" {
  default     = "SSM-SessionManagerRunShell"
  type        = string
  description = "The name of the document."
}

variable "s3_bucket_name" {
  default     = ""
  type        = string
  description = "The name of the bucket."
}

variable "s3_key_prefix" {
  default     = ""
  type        = string
  description = "The prefix for the specified S3 bucket."
}

variable "s3_encryption_enabled" {
  default     = true
  type        = bool
  description = "Specify true to indicate that encryption for S3 Bucket enabled."
}

variable "cloudwatch_log_group_name" {
  default     = ""
  type        = string
  description = "The name of the log group."
}

variable "cloudwatch_encryption_enabled" {
  default     = true
  type        = bool
  description = "Specify true to indicate that encryption for CloudWatch Logs enabled."
}
variable "linux_shell_profile"{
    default = ""
    type    = string
    description = "The ShellProfile to use for linux based machines"
}
variable "tags" {
  default     = {}
  type        = map(string)
  description = "A mapping of tags to assign to all resources."
}