# SSM Document
#
resource "aws_ssm_document" "default" {
  name            = var.ssm_document_name
  document_type   = "Session"
  document_format = "JSON"
  tags            = merge({ "Name" = "${var.ssm_document_name}" }, var.tags)

  content = jsonencode({
    schemaVersion = "1.0"
    description   = "Document to hold regional settings for Session Manager"
    sessionType   = "Standard_Stream"
    inputs = {
      s3BucketName                = var.s3_bucket_name
      s3KeyPrefix                 = var.s3_key_prefix
      s3EncryptionEnabled         = var.s3_encryption_enabled
      cloudWatchLogGroupName      = var.cloudwatch_log_group_name
      cloudWatchEncryptionEnabled = var.cloudwatch_encryption_enabled
      runAsEnabled =  false
      shellProfile                = {
        linux = var.linux_shell_profile == "" ? var.linux_shell_profile : ""
      }
    }
  })
}

# resource "aws_ssm_association" "ssm_bootstrap_assoc" {
#   name                = "${aws_ssm_document.default.name}"
#   schedule_expression = "${var.ssm_association_refresh_rate}"

#   targets {
#     key    = "tag:SSM Target Tag"
#     values = ["Target-${var.ssm_document_name}"]
#   }

#   depends_on = ["aws_ssm_document.default"]
# }

# Session Manager IAM Instance Profile
#


resource "aws_iam_instance_profile" "default" {
  name = "${var.name}-session-manager"
  role = var.iam_name
  path = var.iam_path
}