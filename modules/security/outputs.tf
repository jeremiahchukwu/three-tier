# output "ec2instance" {
#   value = aws_instance.web_server.public_ip
# }
# output "sshkey" {
#   value = tls_private_key.oskey.public_key_openssh
# }


output "app_server" {
  value = aws_security_group.app_server.id
}

output "app_alb" {
  value = aws_security_group.app_alb.id
}
output "rds" {
  value = aws_security_group.rds.id
}
output "ssm" {
  value = aws_security_group.ssm_default.id
}


output "web_server" {
  value = aws_security_group.web_alb.id
}

output "web_alb" {
  value = aws_security_group.web_alb.id
}