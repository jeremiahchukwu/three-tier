#------------------------------------------------------------------------------
# Backend  Web Server ASG, ALB, Security Group Configuration
#------------------------------------------------------------------------------


resource "aws_security_group" "app_server" {
  name   = "${var.name}-app-sg"
  vpc_id = var.vpc_id

  ingress {
    protocol         = "tcp"
    from_port        = var.app_port
    to_port          = var.app_port
    security_groups = ["${aws_security_group.app_alb.id}"]
  }
  # ingress {
  #   protocol         = "tcp"
  #   from_port        = 80
  #   to_port          = 80
  #   security_groups = ["${aws_security_group.app_alb.id}"]
  # }
  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "${var.name}-sg-task"
    # Environment = var.environment
  }
}




resource "aws_security_group" "app_alb" {
  name   = "${var.name}-app-sg-alb"
  vpc_id = var.vpc_id

  # ingress {
  #   protocol         = "tcp"
  #   from_port        = 80
  #   to_port          = 80
  #   cidr_blocks      = ["0.0.0.0/0"]
  #   ipv6_cidr_blocks = ["::/0"]
  # }
  ingress {
    protocol         = "tcp"
    from_port        = var.app_port
    to_port          = var.app_port
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    protocol         = "tcp"
    from_port        = 443
    to_port          = 443
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "${var.name}-sg-alb"
    # Environment = var.environment
  }
}

#------------------------------------------------------------------------------
# Database Security Group Configuration
#------------------------------------------------------------------------------



resource "aws_security_group" "rds" {
  name        = "terraform_rds_security_group"
  description = "Terraform example RDS MySQL server"
  vpc_id      =  var.vpc_id
  # Keep the instance private by only allowing traffic from the web server.
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${aws_security_group.app_alb.id}"]
  }
  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "terraform-example-rds-security-group"
  }
}

#------------------------------------------------------------------------------
# System Manager Security Group Configuration
#------------------------------------------------------------------------------


resource "aws_security_group" "ssm_default" {
  name   = "${var.name}-sg-SSM"
  vpc_id = var.vpc_id

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "${var.name}-sg-SSM"
    # Environment = var.environment
  }
}



#------------------------------------------------------------------------------
# Frontend  Web Server ASG, ALB, Security Group Configuration
#------------------------------------------------------------------------------


resource "aws_security_group" "web_server" {
  name   = "${var.name}-sg-web-server"
  vpc_id = var.vpc_id

  ingress {
    protocol         = "tcp"
    from_port        = var.web_port
    to_port          = var.web_port
    security_groups = ["${aws_security_group.web_alb.id}"]
  }
  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "${var.name}-sg-task"
    # Environment = var.environment
  }
}


resource "aws_security_group" "web_alb" {
  name   = "${var.name}-sg-web-server-alb"
  vpc_id = var.vpc_id

  ingress {
    protocol         = "tcp"
    from_port        = var.web_port
    to_port          = var.web_port
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "${var.name}-sg-alb"
    # Environment = var.environment
  }
}