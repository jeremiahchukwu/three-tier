variable "name" {
  description = "the name of your stack, e.g. \"demo\""
  default = "omini"
}

variable "region" {
  description = "aws region"
  default = "us-east-1"
}

variable "availability_zones" {
  description = "a comma-separated list of availability zones, defaults to all AZ of the region, if set to something other than the defaults, both private_subnets and public_subnets have to be defined as well"
  default     = ["eu-west-1a", "eu-west-1b","eu-west-1c","eu-west-1d"]
}

variable "cidr" {
  description = "the name of your stack, e.g. \"demo\""
  default = "192.168.0.0/16"
}

variable "public_subnets" {
  description = "the name of your stack, e.g. \"demo\""
  default = ["192.168.1.0/24","192.168.3.0/24","192.168.4.0/24"]
}

variable "private_subnets" {
  description = "the name of your stack, e.g. \"demo\""
  default = ["192.168.2.0/24"]
}
